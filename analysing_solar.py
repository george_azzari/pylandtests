import os
from L1TMetadata import *

""" Doing this test I just relaized that there must be a probblem with L7 metadata files. Are they different? Need to check this."""

databasepath = "/media/Data/LANDSAT_P40R37" 
L1Tfolders = os.listdir(databasepath)

solar_az = []
solar_zen = []
dates = []
for folder in L1Tfolders:
    temp_metadata = L1TMetadata(databasepath, folder)
    solar_az.append(temp_metadata.SolarAzimuth)
    solar_zen.append(temp_metadata.SolarZenith)
    dates.append(temp_metadata.AcquisDatetime)
    
print solar_az