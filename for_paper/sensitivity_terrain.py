import sys
sys.path.append("/home/george/PyLandtools/")
from datetime import datetime
from matplotlib import pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import L1P40R37
import L1Terrain

databasepath = "/media/WDBook/P40R37/LT5"
run_param = L1P40R37.L1RunParams_P40R37()
wrp_win = L1P40R37.WarpWindow_StAnaCenter()
scenename = 'LT50400372011169PAC01'

r = np.pi/180.
solz = 45.0*r
solaz = 0.*r
min_slo = 0.*r
max_slo = 70.*r
min_asp = 0.*r
max_asp = 359.*r
n = 100
slo_arr = np.linspace(min_slo, max_slo, n)
asp_arr = np.linspace(min_asp, max_asp, n)


ct = L1Terrain.Costheta(databasepath,scenename,run_param,wrp_win)
td = L1Terrain.TFDirect(databasepath,scenename,run_param,wrp_win)
tfj = L1Terrain.TFDiffuse_LJRD(databasepath,scenename,run_param,wrp_win)
tft = L1Terrain.TFDiffuse_TMCLS(databasepath,scenename,run_param,wrp_win)
tft2 = L1Terrain.TFDiffuse_TMCLS2(databasepath,scenename,run_param,wrp_win)


ct_arr = ct.calc_core(asp_arr, slo_arr, solz, solaz)
td_arr = td.calc_core(ct_arr, solz)
tfj_arr = tfj.calc_core(slo_arr)
tft_arr = tft.calc_core(slo_arr, ct_arr, solz)
tft2_arr = tft2.calc_core(slo_arr,ct_arr, solz)

ct_mtx = np.zeros((n,n))
i = 0
for slo in slo_arr:
    j = 0
    for asp in asp_arr:
        ct_mtx[i,j] = ct.calc_core(asp,slo,solz,solaz)
        j += 1
    i += 1

s, a = np.meshgrid(slo_arr, asp_arr, indexing="ij")

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.plot_surface(s*1./r, a*1./r, ct_mtx, rstride=1, cstride=1, cmap=cm.jet, linewidth=0, antialiased=False)
ax.set_xlabel("Slope [deg]")
ax.set_ylabel("Aspect [deg]")
ax.set_zlabel("Cos(T) [sr-1]")

td_mtx = td.calc_core(ct_mtx, solz)
fig2 = plt.figure()
ax2 = fig2.gca(projection='3d')
ax2.plot_surface(s*1./r, a*1./r, td_mtx, rstride=1, cstride=1, cmap=cm.jet, linewidth=0, antialiased=False)
ax2.set_xlabel("Slope [deg]")
ax2.set_ylabel("Aspect [deg]")
ax2.set_zlabel("Direct Illum. Factor")

tfj_mtx = tfj.calc_core(s)
fig3 = plt.figure()
ax3 = fig3.gca(projection='3d')
ax3.plot_surface(s*1./r, a*1./r, tfj_mtx, rstride=1, cstride=1, cmap=cm.jet, linewidth=0, antialiased=False)
ax3.set_xlabel("Slope [deg]")
ax3.set_ylabel("Aspect [deg]")
ax3.set_zlabel("Diffuse LJ Illum. Factor")


tft_mtx = tft.calc_core(s,ct_mtx, solz)
fig4 = plt.figure()
ax4 = fig4.gca(projection='3d')
ax4.plot_surface(s*1./r, a*1./r, tft_mtx, rstride=1, cstride=1, cmap=cm.jet, linewidth=0, antialiased=False)
ax4.set_xlabel("Slope [deg]")
ax4.set_ylabel("Aspect [deg]")
ax4.set_zlabel("Diffuse TC Illum. Factor")


fig5 = plt.figure()
ax5 = fig5.gca(projection='3d')
ax5.plot_surface(s*1./r, a*1./r, td_mtx, rstride=1, cstride=1, cmap=cm.afmhot, linewidth=0, antialiased=False)
ax5.plot_surface(s*1./r, a*1./r, tft_mtx, rstride=1, cstride=1, cmap=cm.PuBu_r, linewidth=0, antialiased=False)
ax5.set_xlabel("Slope [deg]")
ax5.set_ylabel("Aspect [deg]")
ax5.set_zlabel("Diffuse TC Illum. Factor")


plt.show()