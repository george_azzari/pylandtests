__author__ = 'George'

from PyLand import GeoShared
from PyLand import L1Shared
from PyLand import L1Spectral
from PyLand import L1MultiSpectral
from PyLand import L1Svi
from PyLand import L1TimeSeries
from PyLand import L1TimeSeriesGX
from PyLand.PaperAnalysis import StdArgs
from PyLand.PaperAnalysis import StdPolyNames
from datetime import date, datetime

start_dt = date(1996,1,1)
end_dt = date(1998,1,1)
runid = "SANTIAGOTESTTS2"

ndvitoc_args = dict(component=L1Svi.NDVI, component_type="TOC")
ndvitoc_args.update(databasepath=StdArgs.raster_dir, run_params=StdArgs.get_runparams(runid))

ndvitopo_args = dict(component=L1Svi.NDVI, component_type="TOPO")
ndvitopo_args.update(databasepath=StdArgs.raster_dir, run_params=StdArgs.get_runparams(runid))

ndvitopo_args = dict(component=L1Svi.NDVI, component_type="TOPO")
ndvitopo_args.update(databasepath=StdArgs.raster_dir, run_params=StdArgs.get_runparams(runid))

comp_args_list = [ndvitoc_args, ndvitopo_args]
ndvi_multi_ts = L1TimeSeries.L1MultiComponentTS(start_dt, end_dt, comp_args_list)


tsgx = L1TimeSeriesGX.PlotMaxKDE(ndvi_multi_ts)
StdArgs.plot_all_polys(tsgx, StdPolyNames.trabuco_Ssmall_polys, "NDVIcomp")