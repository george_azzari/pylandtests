__author__ = 'George'

from PyLand import GeoShared
from PyLand import L1Shared
from PyLand import L1Spectral
from PyLand import L1TimeSeries
from datetime import datetime, date
import matplotlib.pyplot as plt
import os


source = "/media/WDBook/P40R37"
raster_dir = "{0}/LT5".format(source)

corr_mode = "single_run"
aeros_model = "urban"
ground_type = "vegetation"
gpu_mode = False
diff_model = "LJRD" # Temp-Coulson model for diffuse light
run_id = "SANTIAGOTESTTS2"
runparam = L1Shared.L1RunParams(run_id, corr_mode, aeros_model, ground_type, diff_model, overwrite=False)
runparam.set_irrad_components(diffuse=True, environm=True)

bottom = 3722444.0228  #mN
top = 3740067.1478 #mN
right = 457690.4655 #mE
left = 422384.5555 #mE
runparam.set_window_borders(left, bottom, right, top, georeferenced=False)

start_dt = date(1996,1,1)
end_dt = date(2009,1,1)

dnwrp = L1TimeSeries.L1TimeSeries(L1Spectral.L1DNWRP, raster_dir, start_dt, end_dt, runparam)
#dnwrp.write_all_multispectral()

radtoa = L1TimeSeries.L1TimeSeries(L1Spectral.RADtoa, raster_dir, start_dt, end_dt, runparam)
#radtoa.write_all_multispectral()

kml_dir = "{0}/GEkml".format(source)
tmp_figs = "/home/george/GDrive/TempFigures"

def get_layername(poly_name):
    return "{0}.kml".format(poly_name)

def get_filepath(poly_name):
    return "{0}/{1}".format(kml_dir, get_layername(poly_name))

#TRABUCO CANYON ANALYSIS
both = "TrabucoBothslopes"
north_big = "TrabucoNortslope"
south_big = "TrabucoSouthslope"
riparian = "TrabucoRiparian"
south_small1 = "TrabucoSsmall1"
south_small2 = "TrabucoSsmall2"
south_small3 = "TrabucoSsmall3"
north_small1 = "TrabucoNsmall1"
north_small2 = "TrabucoNsmall2"
north_small3 = "TrabucoNsmall3"
trabuco_big_polys = [both, north_big, south_big, riparian]
trabuco_Ssmall_polys = [south_small1, south_small2, south_small3]
trabuco_Nsmall_polys = [north_small1, north_small2, north_small3]


def plot_all_trabuco(component_ts, polygons):
    comp_tag = component_ts.scenes[0].band1.component_tag
    img_dir = "{0}/TRABUCOTS{1}".format(tmp_figs, comp_tag)
    if not os.access(img_dir, os.F_OK):  # Check if the destination folder exists already
        os.mkdir(img_dir)  # If not create it
    for poly in polygons:
        ax_topo_ns = component_ts.plot_maxkde_ts_fromployg(get_filepath(poly), get_layername(poly), "KML")
        ax_topo_ns.set_title(poly)
        name_output_file = "{0}/TRABUCOTS.{1}.{2}.png".format(img_dir,comp_tag,poly)
        plt.savefig(name_output_file, dpi=300, facecolor='w', edgecolor='w',orientation='portrait', bbox_inches='tight')

#reftoa = L1TimeSeries.L1TimeSeries(L1Spectral.REFtoa, raster_dir, start_dt, end_dt, runparam)
#reftoa.write_all_multispectral()
#reftoa.plot_maxkde_ts_fromployg(shpfile, layername, driver)

reftoc = L1TimeSeries.L1MultiSpectralTS(L1Spectral.REFtoc, raster_dir, start_dt, end_dt, runparam)
#reftoc.write_all_multispectral()
plot_all_trabuco(reftoc, trabuco_Ssmall_polys)
plot_all_trabuco(reftoc, trabuco_Nsmall_polys)
plot_all_trabuco(reftoc, trabuco_big_polys)

#reftopo = L1TimeSeries.L1TimeSeries(L1Spectral.REFtopo, raster_dir, start_dt, end_dt, runparam)
#reftopo.write_all_multispectral()
#plot_all_trabuco(reftopo, trabuco_Ssmall_polys)
#plot_all_trabuco(reftopo, trabuco_Nsmall_polys)
#plot_all_trabuco(reftopo, trabuco_big_polys)

#refdelta = L1TimeSeries.L1TimeSeries(L1Spectral.REFdelta, raster_dir, start_dt, end_dt, runparam)
#refdelta.write_all_multispectral()
#plot_all_trabuco(refdelta, trabuco_Ssmall_polys)
#plot_all_trabuco(refdelta, trabuco_Nsmall_polys)
#plot_all_trabuco(refdelta, trabuco_big_polys)

#totirr = L1TimeSeries.L1TimeSeries(L1Spectral.TOTIRRADtopo, raster_dir, start_dt, end_dt, runparam)
#totirr.write_all_multispectral()
#plot_all_trabuco(totirr, trabuco_Ssmall_polys)
#plot_all_trabuco(totirr, trabuco_Nsmall_polys)
#plot_all_trabuco(totirr, trabuco_big_polys)


#totirrdelta = L1TimeSeries.L1TimeSeries(L1Spectral.TOTIRRADdelta, raster_dir, start_dt, end_dt, runparam)
#totirrdelta.write_all_multispectral()
#plot_all_trabuco(totirrdelta, trabuco_Ssmall_polys)
#plot_all_trabuco(totirrdelta, trabuco_Nsmall_polys)
#plot_all_trabuco(totirrdelta, trabuco_big_polys)

#plt.show()