import sys
sys.path.append("/home/george/PyLandtools/")

from datetime import datetime
from matplotlib import pyplot as plt
from modis import ORNLDAACplots

grass_mod09a1 = "../../LocalData/P40R37/modis_ascii/MOD09A1_GRASSL.asc"
grass_mcd43a4 = "../../LocalData/P40R37/modis_ascii/MCD43A4_GRASSL.asc"
fsage_mod09a1 = "../../LocalData/P40R37/modis_ascii/MOD09A1_SAGE.asc"
fsage_mcd43a4 = "../../LocalData/P40R37/modis_ascii/MCD43A4_SAGE.asc"
chap_mod09a1 = "../../LocalData/P40R37/modis_ascii/MOD09A1_DESCHAP.asc"
chap_mcd43a4 = "../../LocalData/P40R37/modis_ascii/MCD43A4_DESCHAP.asc"
pinjunip_mod09a1 = "../../LocalData/P40R37/modis_ascii/MOD09A1_PINJUNIP.asc"
pinjunip_mcd43a4 = "../../LocalData/P40R37/modis_ascii/MCD43A4_PINJUNIP.asc"

s = datetime(2001, 1, 1)
e = datetime(2013, 6, 1)
b = 2

ORNLDAACplots.plot_reflts(b, s, e, grass_mod09a1, grass_mcd43a4, title="Grass")
ORNLDAACplots.plot_reflts(b, s, e, chap_mod09a1, chap_mcd43a4, title="Chaparral")
ORNLDAACplots.plot_reflts(b, s, e, chap_mod09a1, chap_mcd43a4, title="Sage")
ORNLDAACplots.plot_reflts(b, s, e, pinjunip_mod09a1, pinjunip_mcd43a4, title="Pinyon-Juniper")


plt.show()



