from datetime import datetime
from matplotlib import pyplot as plt
from modis import LPDAAC

fpath = "/home/george/Dropbox/Python/PyLATSAA/modis_brdf/modis_ascii/MCD43A1_test.asc"
pr = LPDAAC.Product(fpath)


strid = "BRDF_Albedo_Parameters_Band1.Num_Parameters_01"
v = LPDAAC.Variable(strid, scale=0.001)
c = pr.get_csv_gen()
f = list(c)

r = LPDAAC.Line(f[0])
d = r.check_varid(strid)

g = pr.get_var_gen(v)
l = list(g)


s = datetime(2003,1,1)
e = datetime(2013,6,1)
gt = pr.var_gen_daterange(v,s,e)
lt =list(gt)


pr2 = LPDAAC.MCD43A1(fpath)
pr2.plot_timeseries(4,s,e)

plt.show()



