import sys
sys.path.append("/home/george/PyLandtools/")
from datetime import datetime
from matplotlib import pyplot as plt
from modis import ORNLDAACplots

fgrass = "../../LocalData/P40R37/modis_ascii/MCD43A1_GRASSL.asc"
fsage = "../../LocalData/P40R37/modis_ascii/MCD43A1_SAGE.asc"
fchap = "../../LocalData/P40R37/modis_ascii/MCD43A1_DESCHAP.asc"
fpinjunip = "../../LocalData/P40R37/modis_ascii/MCD43A1_PINJUNIP.asc"

s = datetime(2001,1,1)
e = datetime(2013,6,1)
b = 2

ORNLDAACplots.plot_fparams(b, s, e, fgrass, title="Grass")
ORNLDAACplots.plot_fparams(b, s, e, fchap, title="Chaparral")
ORNLDAACplots.plot_fparams(b, s, e, fsage, title="Sage")
ORNLDAACplots.plot_fparams(b, s, e, fpinjunip, title="Pinyon-Juniper")

plt.show()



