# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

# Sunview geometry
from rossli import SunViewGeom, RossLiKernels, Angles

solz = 45.0
solaz = 45.0
viewz = 5.0
viewaz = 0.0
slope = 10.
aspect = 0.
sunview = SunViewGeom.SunViewGeom(solz, viewz, solaz, viewaz)
cp = Angles.CanopyGeom()
# sunview.solz.set_dlt_bndrs(1)
# sunview.solaz.set_dlt_bndrs(1)
# sunview.viewz.set_dlt_bndrs(1)
# sunview.viewaz.set_dlt_bndrs(1)
sunview.solz.set_bndrs(10, 30)
sunview.solaz.set_bndrs(0, 180)
sunview.viewz.set_bndrs(-80., 80.)
sunview.viewaz.set_bndrs(0, 180)
# sunview.viewaz.set_single_bndrs()

# l1 = sunview.looper(3, 2, var="solar", get_angles=True)
# for gm in l1 :
#     print gm
#     # print sunview.solaz.deg


z, a = sunview.get_meshgrid(10,10,"solar")
k = RossLiKernels.RossLiKernels(sunview, cp)
l2 = k.klooper(3,2, var="solar", get_table=True)
# for m in l2:
#     print m

# vk = RossLiKernels.KernelsClassicVis(k)
# vk.plot_1d(100)
# plt.show()
# for m in l:
#     print m[0]
#
# print l[:,0]
# f_iso_b2 = 227
# f_geo_b2 = 163
# f_vol_b2 = 36
# b = RossLiBRDF(f_iso_b2,f_vol_b2,f_geo_b2,sunview)
# lb = b.brlooper(2,2, var = "solar", get_values=True)
#
# for val in lb:
#     print val

l3 = k.klooper(1, 20, var="solar", get_table=True)
for n in l3:
    indx = [0,1,6,7]
    print n[indx]

l4 = k.klooper(20, 1, var="view", get_table=True)
for n in l4:
    indx = [2,3,6,7]
    print n[indx]

pa = RossLiKernels.PhaseAngle(sunview, cp)
pa.calc()
print  pa.svgeom.relaz.cos, pa.deg, pa.eqdeg
# l5 = pa.palooper(1, 20, var = "solar", get_table=True)
# for n in l5:
#     indx = [0,1,4,7,8]
#     print n[indx]

sunview2 = sunview.get_copy()
sunview2.solaz.update(200.)
sunview2.update_relaz()
pa2 = RossLiKernels.PhaseAngle(sunview2, cp)
pa2.calc()
print pa2.svgeom.relaz.cos, pa2.deg, pa2.eqdeg



# todo: does looper in sunview need to be copied somehow? If I print l2 and l3, then l3 seems to have same size of l2...
# todo: kernels seem definitely insensitive to azimuth again... wtf??