from datetime import date

import matplotlib.pyplot as plt
import numpy as np

import SunViewGeom
import BRDFVisual



# Sunview geometry
from rossli import RossLiBRDF

conv  = np.pi/180.
solz = 45.0
solaz = 150.0 * conv
viewz = 0. * conv
viewaz = 0. * conv
slo = 50 * conv
asp = 160. * conv

#BRDF
f_iso_b2 = 227
f_geo_b2 = 163
f_vol_b2 = 36
brdf = RossLiBRDF.RossLiBRDF(f_iso_b2, f_geo_b2, f_vol_b2)

databasepath = "/home/george/LandsatData/P40R37_SOURCES"
startd = date(1995,1,1)
endd = date(2000,1,1)
brdf_ts = BRDFVisual.BRDFTimeSeries(databasepath, startd, endd, brdf, slo, asp)
for rows in brdf_ts.brdf_ts:
    print rows
brdf_ts.plot_ts()
plt.show()