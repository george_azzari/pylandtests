# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

import matplotlib.pyplot as plt

# Sunview geometry
from rossli import RossLiBRDF

solz = 70.0
solaz = 45.0
viewz = 0.0
viewaz = 0.0
canopy = RossLiBRDF.CanopyGeom()
sunview = RossLiBRDF.SunViewGeom(solz, viewz, solaz, viewaz)
# sunview.solz.set_dlt_bndrs(1)
# sunview.solaz.set_dlt_bndrs(1)
# sunview.viewz.set_dlt_bndrs(1)
# sunview.viewaz.set_dlt_bndrs(1)
sunview.solz.set_bndrs(0, 90)
sunview.solaz.set_bndrs(0, 180)
sunview.viewz.set_bndrs(0, 90)
sunview.viewaz.set_bndrs(0, 180)

#BRDF
f_iso_b2 = 227
f_geo_b2 = 163
f_vol_b2 = 36
brdf = RossLiBRDF.RossLiBRDF(f_iso_b2, f_geo_b2, f_vol_b2)
#print "BRDF ", brdf.calc_expl(solz, solaz, viewz, viewaz), brdf.calc(kernels)

# BRDF View
# view = MODISbrdf.ValidationView(canopy, solz, solaz, viewaz)
# view.plot_kernels()
# plt.show()
vis = RossLiBRDF.Visualize(canopy, sunview, brdf)
vis.plot_kernels()
vis.plot_brdf()
vis.plot_intgr()
plt.show()
#

# #Kernels
# kernels = MODISbrdf.RossLiKernels(sunview, canopy)
# print "\nKvol ", kernels.kvol
#



# Integrand function
integrand = RossLiBRDF.IntegrandRad(brdf)
#
# iterat = 20
# #Integration with Quad
# quad_rad = MODISbrdf.SingleQuadratureRad(integrand, sunview, 1e-3, iterat)
# res, err = quad_rad.integ_tot()
# print "Single Quad integration with ", iterat, " itearations. Result = ", res, " Error ", err, "\n"

# #Integration with Doublequad:
# dblq_rad = MODISbrdf.MultiQuadratureRad(integrand, sunview, 1e-3)
# res, err = dblq_rad.integ_tot()
# print "Two double Quad integration with ", iterat, " itearations. Result = ", res, " Error ", err, "\n"

#Montecarlo integration
iterat = 1000
mntc_rad = RossLiBRDF.MontecarloRad(integrand, sunview, iterat)
mnt_int, error = mntc_rad.integrate()
print "Montecarlo integration with ", iterat, " iterations. Result = ", mnt_int, " Error ", error, "\n"


