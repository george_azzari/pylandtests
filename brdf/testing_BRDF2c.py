import matplotlib.pyplot as plt
import numpy as np

import SunViewGeom
import BRDFVisual


# Sunview geometry
from rossli import RossLiBRDF

conv  = np.pi/180.
solz = 45.0 * conv
solaz = 150.0 * conv
viewz = 0. * conv
viewaz = 0. * conv
slo = 20 * conv
asp = 290. * conv
canopy = SunViewGeom.CanopyGeom()
sunview = SunViewGeom.SunViewGeom(solz, viewz, solaz, viewaz)
tpgsunview = SunViewGeom.TopogSunViewGeom(solz, viewz, solaz, viewaz, slo, asp)
# sunview.set_illrange_direct()
# sunview.set_viewrange_landsat()
# sunview.viewz.mindeg
# angles = sunview.angles_list()
sz = round(sunview.solz.deg, 1)
sa = round(sunview.solaz.deg,1)
vz = round(sunview.viewz.deg,1)
va = round(sunview.viewaz.deg,1)
tsz = round(tpgsunview.solz.deg, 1)
tsa = round(tpgsunview.solaz.deg,1)
tvz = round(tpgsunview.viewz.deg,1)
tva = round(tpgsunview.viewaz.deg,1)
print "\nSUMMARY: (slope = {0}, aspect = {1})".format(slo/conv, asp/conv)
print "Flat: || solar-zenith:{0} | solar-azimuth:{1} | view-zenith:{2} | view-azimuth:{3} ||".format(sz, sa, vz, va)
print "Topo: || solar-zenith:{0} | solar-azimuth:{1} | view-zenith:{2} | view-azimuth:{3} ||".format(tsz, tsa, tvz, tva)
print "\n"

#BRDF
f_iso_b2 = 227
f_geo_b2 = 163
f_vol_b2 = 36
brdf = RossLiBRDF.RossLiBRDF(f_iso_b2, f_geo_b2, f_vol_b2)


vis = BRDFVisual.VisTopography(sunview, brdf)
vis.set_slope_range(0., 40.)
vis.set_aspect_range(0., 360.)
vis.calc_matrix()
vis.plot_matrix()
vis.plot_aspect(99)
vis.plot_slope(1)

vis_sun = BRDFVisual.VisSolar(slo, asp, brdf)
vis_sun.set_solz_range(0., 80.)
vis_sun.set_solaz_range(0., 360.)
vis_sun.calc_matrix()
vis_sun.plot_matrix()
vis_sun.plot_azimuth(99)
vis_sun.plot_zenith(1)
plt.show()