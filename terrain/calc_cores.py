import sys
sys.path.append("/home/george/PyLandtools/")
from datetime import datetime
from matplotlib import pyplot as plt
import L1P40R37
import L1Terrain

databasepath = "/media/WDBook/P40R37/LT5"
run_param = L1P40R37.L1RunParams_P40R37()
wrp_win = L1P40R37.WarpWindow_StAnaCenter()
scenename = 'LT50400372011169PAC01'

ct = L1Terrain.Costheta(databasepath,scenename,run_param,wrp_win)
ct.create_new()

td = L1Terrain.TFDirect(databasepath,scenename,run_param,wrp_win)
td.create_new()

tfj = L1Terrain.TFDiffuse_LJRD(databasepath,scenename,run_param,wrp_win)
tfj.create_new()

tft = L1Terrain.TFDiffuse_TMCLS(databasepath,scenename,run_param,wrp_win)
tft.create_new()

tft2 = L1Terrain.TFDiffuse_TMCLS2(databasepath,scenename,run_param,wrp_win)
tft2.create_new()